import React, { Component } from 'react';
import { injectIntl } from 'react-intl';
import { inject, observer } from 'mobx-react/index';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Container } from './../css';
import OutsideClickHandler from 'react-outside-click-handler';

const ContainerFooter = styled.div`
  color: white;
  background: linear-gradient(41deg, #0C4146 0%, #48A1B0 100%);
  height: 187px;
`;

const RowContainerIcons = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  
  .links {
    flex: 0;
    margin-left: 20px;  
  }
  .links.links-flex {
    flex: 0;
  }
`;
const FooterText = styled.span`
  font-size: 13px;
  text-align: left;
  margin-top: 24px;
  height: 15px;
  width: 203px;
  font-size: 13px;
  line-height: 15px;
  font-family: Rubik;
`;
const Spacer = styled.div`
  @media (max-width: 700px) {
    display: none;
  }
  flex: 1;
`;

const RowContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 14px; 
  @media (max-width: 700px) {
    flex-direction: column;
  }
  @media (min-width: 700px) {
  }
  a:hover,
  a:visited,
  a:link,
  a:active {
    text-decoration: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
  }
`;

const DropdownButton = styled.div`
  cursor: pointer;
  flex: 1;
  height: 49px;	
  width: 190px;	
  min-width: 160px;
  border-radius: 8px;	
  background-color: #17D1AA;	
  margin-bottom:5px;
  box-shadow: 0 2px 48px 0 rgba(83,81,81,0.5);
  color: #ffffff;
  display: block;
  overflow:hidden;
  
  .ArrowUp {
    margin-left: 158px;
    margin-top: -31px;
    transform: rotate(180deg);
  }
 `;

const DownloadButtonText = styled.div`
  margin-left: 15px; 
  font-size: 15px;
  font-weight: 500; 
  margin-top: 5px;
  text-transform: uppercase;
`;

const DropdownContent = styled.div`
  position: absolute;
  background-color: white;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  border-radius: 8px;	
  z-index: 3;
  top: 100%;
  bottom: auto;
  margin-bottom: 5px;
  overflow: auto;
  text-transform:initial;
  text-align: justify;
  width: 190px;

  a {
    background-color: #17d1aa;
    padding: 5px 5px;
    display: block;
    font-size: 14px;
    cursor: pointer;
  }
  a:hover {
    background-color: #14E2B8
  }
  .icon {
    width: 24px;
    height: 24px;
    vertical-align: middle;
    margin-right: 5px;
  }
  .icon.icon-shrink {
    width: 16px;
    height: 16px;
    vertical-align: middle;
    margin-right: 10px;
    margin-left: 5px;
  }
`;

const Download = styled.div`
  position: relative;
  display: inline-block;
  color: #FFFFFF;	
  font-family: Rubik;	
  font-size: 15px;	
  font-weight: initial;	
  line-height: 40px; 
  flex: 0.8;

  margin-top: 60px;
  :hover ${DropdownButton} {
    background-color: #14E2B8;
    align-items:left;
  }
`;

const DownloadItemImage = styled.img`
  width: 24px;
  height: 24px;
  vertical-align: middle;
  margin-right: 5px;
`;

const LogoSize = styled.div`
  margin-top: 43px;
`;

const Logo = styled.img`
  font-size: 1.5em;
  text-align: center;
  width: 188.19px;
  height: 35px;
  margin-right: -110px;
  margin-left: -30px;
`;

const LinkS = styled.div`
  flex: 0.5;
  font-weight: 500;
  font-size: 14px;
  margin: 2px;
  text-align: right;
  margin-top: -22px;
 
  @media (max-width: 700px) {
  }
  a:visited {
    color: white;
  }

  a:link {
    color: white;
  }
  `;

const ContainerHeight = styled(Container)`
  @media (min-width: 700px) {
    height: 187px;
  }
`;

const Dropdown = styled.div`
  position: relative;
  display: inline-block;
  color: white;
  font-family: Rubik;
  font-size: 15px;
  font-weight: initial;
  margin-top: -10px;
  flex: 0.8;
`;

const SelectButton = styled.label`
  cursor: pointer;
  display: flex;
  height: 190px;
  width: 100px;
  color: white;
  flex-direction: row;
`;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      showDropdown: false,
      lang: 'en'
    };
  }

  closePopUp = () => {
    this.setState({ show: false });
  };

  /* Single page app - scroll to top, to look like a normal webpage */
  scroll = () => {
    window.scrollTo(0, 0);
  };

  hideDropdownMenu = () => {
    this.setState({ showDropdown: false });
  };

  showDropdownMenu = () => {
    this.setState({ showDropdown: true });
  };

  setLocale = (event, locale) => {
    locale.value = event.target.name;
  };

  render() {
    const lang = (locale) => {
      if (locale.value === 'ja') {
        return 'Jap';
      }
      return 'Eng';
    };
    const _Footer = ({ locale, intl: { formatMessage } }) => (
      <ContainerFooter >
        <ContainerHeight>
          <RowContainer>
            <LogoSize>
              <Logo src="./assets/EMURGOTEST-logo.svg" style={{ width: '188.19px'}} />
            </LogoSize>
            <Spacer />
            <LinkS style={{display: 'flex', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'flex-end', marginTop: '50px' }}>
              <LinkS onScroll={this.scroll}>
                <Link to="/about">
                  {formatMessage({ id: 'header.about' })}
                </Link>
              </LinkS>
              <LinkS onScroll={this.scroll}>
                <Link to="/blog">
                  {formatMessage({ id: 'home.blog.title' })}
                </Link>
              </LinkS>
              <LinkS onScroll={this.scroll}>
                <Link to="/contact">
                  {formatMessage({ id: 'header.contact' })}
                </Link>
              </LinkS>
              <LinkS onScroll={this.scroll} onClick={this.showDropdownMenu}>
                <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                  <Link to="" style={{cursor: 'pointer'}}>
                    {lang(locale)}
                  </Link>
                  <img src="./assets/arrow-down.svg" style={{ marginLeft: '10px' }} />
                </div>
                <Dropdown>
                {
                  this.state.showDropdown &&
                  <OutsideClickHandler onOutsideClick={this.hideDropdownMenu}>
                    <DropdownContent>
                      <a className="dropdown-link" name="en" onClick={e => {this.setLocale(e, locale)}}>English</a>
                      <a className="dropdown-link" name="ja" onClick={e => {this.setLocale(e, locale)}}>Japanese</a>
                    </DropdownContent>
                  </OutsideClickHandler>
                }
                </Dropdown>
              </LinkS>
            </LinkS>
          </RowContainer>
          <Spacer />
          <RowContainerIcons style={{paddingTop: '29.74px' }}>
            <FooterText>
              {formatMessage({ id: 'footer.all-rights' })}
            </FooterText>
            <Spacer/>
            <LinkS style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'flex-end', marginTop: '24px' }}>
              <LinkS onScroll={this.scroll} style={{flex: '0.4'}}>
                <img src="./assets/facebook.svg" style={{ width: '24px', height: '24.93px' }} />
              </LinkS>
              <LinkS onScroll={this.scroll} style={{flex: '0.4'}}>
                <img src="./assets/twitter.svg" style={{ width: '24px', height: '24.93px' }} />
              </LinkS>
              <LinkS onScroll={this.scroll} style={{flex: '0.4'}}>
                <img src="./assets/youtube.svg" style={{ width: '24px', height: '24.93px' }} />
              </LinkS>
              <LinkS onScroll={this.scroll} style={{flex: '0.4'}}>
                <img src="./assets/medium-size.svg" style={{ width: '24px', height: '24.93px'}} />
              </LinkS>
              <LinkS onScroll={this.scroll} style={{flex: '0.4'}}>
                <img src="./assets/reddit.svg" style={{ width: '24px', height: '24.93px'}} />
              </LinkS>
              <LinkS onScroll={this.scroll} style={{flex: '0.4'}}>
                <img src="./assets/linkedin.svg" style={{ width: '24px', height: '24.93px'}} />
              </LinkS>
            </LinkS>
          </RowContainerIcons>
        </ContainerHeight>
      </ContainerFooter>
    );

    const Footer = inject('locale')(injectIntl(observer(_Footer)));

    return <Footer />;
  }
}

export default App;
