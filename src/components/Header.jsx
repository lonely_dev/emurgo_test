import React, { Component } from 'react';
import { injectIntl } from 'react-intl';
import { inject, observer } from 'mobx-react/index';
import { Link, NavLink } from 'react-router-dom';
import styled from 'styled-components';
import SweetAlert from 'react-bootstrap-sweetalert';
import OutsideClickHandler from 'react-outside-click-handler';
import { Container } from './../css';

import Plx from "react-plx";
import Download from './Download';

const data = [
  {
    start: 0,
    end: 400,
    properties: [
      {
        startValue: 0,
        endValue: 0,
        property: "opacity"
      }
    ]
  },
  {
    start: 400,
    duration: 100,
    properties: [
      {
        startValue: 0,
        endValue: 1,
        property: "opacity"
      }
    ]
  },

];

const navFixed = {
  width: '100%',
  height: 60,
  backgroundImage: "linear-gradient(82deg,#1a44b7,#4760ff 30%)",
  color: "#fff",
  left: 0,
  top: 0,
  position: "fixed",
  zIndex: 10,
  
};

const Content = styled.div`  
  display: flex;
  max-width: 1115px;
  margin: 10px auto;
  a{
    padding-top: 5px;
    
  }
`;

const NavFixed = styled.div`
  flex: 1;
  text-align: center;
  text-transform: uppercase;
  line-height: 21px;
  font-size: 15px;
  height: 24px;
  font-weight: 500;
  margin: 5px;
  a{    
    display: inline-block;
    text-decoration: none;
    height: 23px;
    img{
      margin-top: -5px;
      margin-left: -65px
    }
    &.active{
      border-bottom: 2px solid white;
    }
    &:link,
    &:visited{
      color: white;
    }
  }
`;

const Logo = styled.img`
  font-size: 1.5em;
  text-align: center;
  width: 200.54px;
  height: 35px;
 
`;

const VTitleList = styled.div`
  flex: 1;
  margin: 4px;
  text-align: center;
 
  font-size: 15px;
  height: 24px;
  font-weight: 500;

  a{
    text-decoration: none;
    display: inline-block;
    height: 23px;

    &:link,   
    &:visited{
      color: white;
    }
  }
  
`;

/*
 * Background<A,B>
 *
 * @media rules
 * to move blue triangle header responsive
 * an correctly centered.
 *
 * .hasOffset is for different sections, that require
 * diffrent sizes.
 */
const Background = styled.div`
  position: relative;
  width: 2961px;
  height: ${p => (p.hasOffset ? '1062px' : '800px')};
  z-index: -1;
  transform: scaleX(-1);

  @media (min-width: 700px) {
    height: ${p => (p.hasOffset ? '1062px' : '800px')};
    top:  ${p => (p.hasOffset ? '-1275px' : '-200')};
    right: ${p => (p.hasOffset ? '640px' : '-90')};
    left: -200;
  }
  @media (max-width: 700px) {
    height: ${p => (p.hasOffset ? '1062px' : '800px')};
    top: -867px;
    right: 23px;
  }

  
  border-radius: 30px;
  background-image: ${p => (p.hasOffset ? 'linear-gradient(82deg, #1a44b7, #4760ff 30%)' : 'url("./assets/start page.jpg")')};
  background-size: 100% 100%;
  background-repeat: no-repeat;
  background-position: center;
`;

const BackgroundB = styled.div`
  position: relative;
  width: 2450px;
  height: ${p => (p.hasOffset ? '270px' : '0')};
  border-radius: 30px;
  transform: ${p => p.hasOffset ? 'skew(52deg, -31deg)' : 'skew(90deg, -31deg)'};
  background-image: ${p => (p.hasOffset ? 'linear-gradient(82deg, #3256C8, #4760ff 30%)' : 'linear-gradient(51.8deg, #3154CB 0%, rgba(49,84,203,0.97) 10%, rgba(49,84,203,0.87) 25%, rgba(49,84,203,0.71) 43%, rgba(49,84,203,0.49) 63%, rgba(49,84,203,0.21) 85%, rgba(49,84,203,0) 100%)')};
  z-index: -2;
  
  
  @media (min-width: 700px) {
    height: ${p => (p.hasOffset ? '926px' : '0')};
    top:  ${p => (p.hasOffset ? '-2292px' : '-1880px')}; 
    right: ${p => (p.hasOffset ? '621px' : '680px')}; 
  }
  @media (max-width: 700px) {
    height: ${p => (p.hasOffset ? '926px' : '912px')};
    top:  ${p => (p.hasOffset ? '-696px' : '-933px')}; ;
    right: -31px;
  }
  
}
`;

const BackgroundC = styled.div`
  position: relative;
  height: ${p => (p.hasOffset ? '727px' : '0')};
  width: 2100px;
  border-radius: 30px;
  transform: ${p => p.hasOffset ? 'skew(52deg, -31deg)' : 'skew(90deg, -52deg)'};
  background: ${p => (p.hasOffset ? 'linear-gradient(27deg, rgba(255,255,255,0) 0%, #FFFFFF 100%)' : 'linear-gradient(49.34deg, #4968D6 0%, rgba(69,101,212,0.95) 10%, rgba(59,92,207,0.81) 42%, rgba(52,87,204,0.73) 69%, rgba(50,85,203,0.7) 88%, rgba(50,85,203,0.61) 89%, rgba(49,84,203,0.44) 92%, rgba(49,84,203,0.31) 95%, rgba(49,84,203,0.23) 98%, rgba(49,84,203,0.2) 100%);')}; 
  z-index: -1;

  @media (min-width: 700px) {
    top: ${p => (p.hasOffset ? '-3090px' : '-2650px')};
    height: ${p => (p.hasOffset ? '727px' : '0')};
    right: ${p => (p.hasOffset ? '480px' : '560px')};
  }

  @media (max-width: 700px) {
    top: ${p => (p.hasOffset ? '-3090px' : '-1343px')};
    height: ${p => (p.hasOffset ? '727px' : '736px')};
    right: 480px;
  }
`;

// const HomeBackground = styled.div`
//   position: relative;
//   width: 2450px;
//   height: ${p => (p.hasOffset ? '2650px' : '0px')};
//   background-color: green;
// `;

const HeaderText = styled.span`
  @media (max-width: 700px) {
    display: none;
  }
  position: relative;
  color: white;
  display: flex;
  margin-top: 15px;
  align-items: flex-end;
  justify-content: flex-end;
`;

const Spacer = styled.div`
  @media (max-width: 700px) {
    display: none;
  }
  flex: 1 auto;
`;

const HContainer = styled.div`
  margin: 0 auto;
  max-width: 1115px;
  height: 88px;
`;

const RowContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  @media (max-width: 700px) {
    flex-direction: column;
  }
`;

const LinkContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: flex-end;
  @media (max-width: 700px) {
    flex-direction: column;
  }
  a{
    text-decoration: none;
    display: inline-block;
    height: 23px;
    font-family: Rubik;

    &:link,   
    &:visited{
      color: white;
    }
  }
`;


const Selector = styled.select`
  background: transparent;
  border: none;
  appearance: caret;
  color: white;
  font-size: 15px;
  font-weight: 500;
  line-height: 18px;
  padding-left: 16px; 
  height: 48px;
  width: 182px;                
  cursor: pointer;
  &:hover{    
    background-color: rgba(255,255,255,0.15);
    box-shadow: 0 2px 48px 0 rgba(83,81,81,0.5);
    
      }
`;

const HeaderAlt = styled.div`
  @media (min-width: 700px) {
    display: none;
  }
  padding-top: 20px;
  align-items: center;
  justify-content: center;
  display: flex;
`;

const HeaderAltSub = styled.div`
  @media (min-width: 700px) {
    display: none;
  }
  width: 100%;
  height: 200px;
  background-image: linear-gradient(82deg, #1a44b7, #4760ff 30%);
  position: absolute;
  padding-top: 40px;
  padding-bottom: 40px;
  align-items: center;
  justify-content: center;
  display: flex;
  flex-direction: column;
  z-index: -100;
`;

const Dropdown = styled.div`
  position: relative;
  display: inline-block;
  color: white;
  font-family: Rubik;
  font-size: 15px;
  font-weight: initial;
  margin-top: -10px;
  flex: 0.8;
`;

const DropdownContent = styled.div`
  position: absolute;
  background-color: white;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  border-radius: 8px;	
  z-index: 3;
  top: 100%;
  bottom: auto;
  margin-bottom: 5px;
  overflow: auto;
  text-transform:initial;
  text-align: justify;
  width: 190px;

  a {
    background-color: #17d1aa;
    padding: 5px 5px;
    display: block;
    font-size: 14px;
    cursor: pointer;
  }
  a:hover {
    background-color: #14E2B8
  }
  .icon {
    width: 24px;
    height: 24px;
    vertical-align: middle;
    margin-right: 5px;
  }
  .icon.icon-shrink {
    width: 16px;
    height: 16px;
    vertical-align: middle;
    margin-right: 10px;
    margin-left: 5px;
  }
`;


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: 1,
      show: false,
      dropdownShown: false
    };
  }

  isThisHomePage() {
    return this.props.location.pathname === '/';
  }

  showDropdown = () => {
    this.setState({ dropdownShown: true });
    console.log('shown');
  }

  hideDropdown = () => {
    this.setState({ dropdownShown: false });
  }

  setLocale = (event, locale) => {
    locale.value = event.target.name;
  }

  render() {
    // Do not show Download on the home page
    const downloadVisibility = this.isThisHomePage() ? 'hidden' : 'visible';

    const offsetPaths = ['/about', '/faq', '/support', '/terms_and_conditions'];
    const hasOffset = offsetPaths.some(r => window.location.href.match(r));
    // const isHome = window.location.href.endsWith('/');
    const lang = (locale) => {
      if (locale.value === 'ja') {
        return 'Jap';
      }
      return 'Eng';
    };
    const _Header = ({ locale, intl: { formatMessage } }) => (
      <HContainer>
        <Container>
          <RowContainer>
            <Link to="/">
              <Logo src="./assets/EMURGOTEST-logo.svg" style={{ width: '188.19px' }} />
            </Link>
            <Spacer />
            <LinkContainer>
              <Link to="/about" style={{ flex: '0.5', margin: '12px' }}>
                {formatMessage({ id: 'header.about' })}
              </Link>
              <Link to="/blog" style={{ flex: '0.5', margin: '12px' }}>
                {formatMessage({ id: 'home.blog.title' })}
              </Link>
              <Link to="/contact" style={{ flex: '0.5', margin: '12px'}}>
                {formatMessage({ id: 'header.contact' })}
              </Link>
              <div 
                style={{display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', flex: '0.5', margin: '12px' }}
                onClick={this.showDropdown}
              >
                <Link to="" style={{cursor: 'pointer'}}>
                  {lang(locale)}
                </Link>
                <img src="./assets/arrow-down.svg" style={{marginLeft: '10px'}} />
                <Dropdown>
                  {
                    this.state.dropdownShown &&
                    <OutsideClickHandler onOutsideClick={this.hideDropdown}>
                      <DropdownContent>
                        <a className="dropdown-link" name="en" onClick={e => {this.setLocale(e, locale)}}>English</a>
                        <a className="dropdown-link" name="ja" onClick={e => {this.setLocale(e, locale)}}>Japanese</a>
                      </DropdownContent>
                    </OutsideClickHandler>
                  }
                </Dropdown>
              </div>
            </LinkContainer>
          </RowContainer>
        </Container>
        <Background hasOffset={hasOffset} />
        <BackgroundB hasOffset={hasOffset} />
        <BackgroundC hasOffset={hasOffset} />
        {/* <HomeBackground hasOffset={isHome} /> */}
      </HContainer>
    );

    const Header = inject('locale')(injectIntl(observer(_Header)));

    return <Header />;
  }
}
export default App;
