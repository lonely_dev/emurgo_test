import React, { Component } from 'react';
import { injectIntl } from 'react-intl';
import { inject, observer } from 'mobx-react';
import styled from 'styled-components';

import { SectionTitle, Container } from '../css';

const Input = styled.input`
  width: 469px;
  height: 54px;
  box-sizing: border-box;
  border: 1px solid #215F68;
  border-radius: 2px;
  margin-left: ${p => (p.marginLeft)};
  margin-right: ${p => (p.marginRight)};
  margin-top: ${p => (p.marginTop)};
  margin-bottom: ${p => (p.marginBottom)};
  padding: ${p => (p.padding)};
  @media (max-width: 700px) {
    width: 60%;
    height: 54px;
  }
`;

const TextArea = styled.textarea`
  box-sizing: border-box;
  height: 170px;
  width: 469px;
  border: 1px solid #215F68;
  border-radius: 2px;
  margin: ${p => (p.margin)};
  padding: ${p => (p.padding)};
`;

const SubmitButton = styled.button`
  width: 216px;
  height: 48px;
  box-sizing: border-box;
  border: 2px solid #215F68;
  border-radius: 2px;
  box-shadow: 0 2px 48px 0 rgba(83, 81, 81, 0.5);
  text-transform: uppercase;
`;

const FlexDiv = styled.div`
  display: flex;
  flex-direction: row;
  @media (max-width: 700px) {
    flex-direction: column;
  }
  align-items: center;
  justify-content: center;
  margin-top: 50px;
`;

const ContactForm = ({ formatMessage }) => (
  <div
    style={{
      backgroundColor: '#F1F7FA',
      width: '700px',
      height: '632px'
    }}
  >
    <form
      style={{
        textAlign: 'center',
        marginTop: '50px'
      }}
    >
      <span
        style={{
          width: '100%',
          height: '28px',
          color: '#1D1E21',
          fontFamily: 'Rubik',
          fontSize: '18px',
          lineHeight: '28px',
          marginLeft: '116px',
          marginTop: '50px',
          marginRight: '97px',
          marginBottom: '16px'
        }}
      >
        {formatMessage({ id: 'home.contact.ask' })}
      </span>
      <Input placeholder="Name" marginLeft="115px" marginRight="115px" marginTop="16px" padding="16px 23px 18px" />
      <Input placeholder="Email" marginLeft="115px" marginRight="115px" marginTop="36px" marginBottom="36px" padding="16px 23px 18px" />
      <span
        style={{
          width: '185px',
          height: '28px',
          color: '#1D1E21',
          fontFamily: 'Rubik',
          fontSize: '18px',
          lineHeight: '28px',
          marginTop: '36px',
          marginBottom: '16px',
          marginLeft: '116px',
          marginRight: '399px'
        }}
      >
        {formatMessage({ id: 'home.contact.how' })}
      </span>
      <TextArea placeholder="Your message" margin="16px 116px 36px" padding="16px 23px 134px" />
      <SubmitButton>
        <span
          style={{
            height: '17px',
            width: '108px',
            color: '#215F68',
            fontFamily: 'Rubik',
            letterSpacing: '1.87px',
            fontWeight: '500',
            fontSize: '14px',
            lineHeight: '17px',
            margin: '16px 54px'
          }}
        >
          {formatMessage({ id: 'home.contact.submit' })}
        </span>
      </SubmitButton>
    </form>
  </div>
);

const ContactImage = () => (
  <div
    style={{
      backgroundColor: '#D8D8D8',
      width: '700px',
      height: '632px'
    }}
  >
    <img src="./assets/Contact us.jpg" style={{ width: 'inherit', height: '632px' }} />
  </div>
);

const FlexedItems = ({ formatMessage }) => (
  <FlexDiv
  >
    <ContactForm formatMessage={formatMessage} />
    <ContactImage />
  </FlexDiv>
);

const MainSection = ({ formatMessage }) => (
  <Container>
    <SectionTitle>
      {formatMessage({ id: 'home.contact.title' })}
    </SectionTitle>
    <FlexedItems formatMessage={formatMessage} />
  </Container>
);

class Contact extends Component {
  render() {
    const _Contact = ({intl: { formatMessage }}) => (
      <MainSection formatMessage={formatMessage} />
    );
    const Contact = inject('locale')(injectIntl(observer(_Contact)));
    return <Contact />;
  }
}

export default Contact;
