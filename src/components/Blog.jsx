import React, { Component } from 'react';
import { injectIntl } from 'react-intl';
import {inject, observer } from 'mobx-react';
import styled from 'styled-components';

import { SectionTitle, Container } from '../css';

const GreyText = styled.span`
  font-family: Rubik;
  width: 76px;
  height: 20px;
  font-size: 14px;
  color: #8c8c8c;
  line-height: 20px;
  margin-top: 30px;
`;

const SubTitle = styled.div`
  height: 44px;
  width: 429px;
  color: #1D1E21;
  font-family: Rubik;
  font-size: 18px;
  line-height: 22px;
  text-align: center;
`;

const Description = styled.span`
  width: ${p => (p.width)};
  height: ${p => (p.height)};
  font-size: ${p => (p.size)};
  font-family: Rubik;
  font-weight: 500;
  line-height: 28px;
  color: #1D1E21;
  margin-top: 15px;
  margin-bottom: ${p => (p.marginBottom)};
`;

const FlexedItems = ({ formatMessage }) => (
  <Container>
    <SectionTitle>
      {formatMessage({ id: 'home.blog.title' })}
    </SectionTitle>
    <div
      style={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center'
      }}
    >
      <SubTitle>
        {formatMessage({ id: 'home.blog.subtitle' })}
      </SubTitle>
    </div>
    <div
    style={{
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: '39px'
    }}
  >
    <div style={{ marginLeft: '31px', marginBottom: '150px', display: 'flex', flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-start', height: '300px', width: '373px'}}>
      <img style={{ width: '370px', height: '250px', marginTop: '12px' }} src="./assets/Blog 1.jpg" />
      <GreyText>
        {formatMessage({ id: 'home.blog.firstDate' })}
      </GreyText>
      <Description height="84px" width="370px" size="18px" marginBottom="50px">
        {formatMessage({ id: 'home.blog.firstDesc' })}
      </Description>
    </div>
    <div style={{ marginLeft: '31px', marginBottom: '175px', display: 'flex', flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-start', height: '263px', width: '395px' }}>
      <img style={{ width: '370px', height: '250px', marginTop: '13px' }} src="./assets/Blog 2.jpg" />
      <GreyText>
        {formatMessage({ id: 'home.blog.secondDate' })}
      </GreyText>
      <Description height="59px" width="341px" size="18px" marginBottom="25px">
        {formatMessage({ id: 'home.blog.secondDesc' })}
      </Description>
    </div>
    <div style={{ marginLeft: '31px', marginBottom: '150px', display: 'flex', flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-start', height: '252px', width: '447px' }}>
      <img style={{ width: '370px', height: '250px' }} src="./assets/Blog 3.jpg" />
      <GreyText>
        {formatMessage({ id: 'home.blog.thirdDate' })}
      </GreyText>
      <Description height="84px" width="362px" size="18px" marginBottom="50px">
        {formatMessage({ id: 'home.blog.thirdDesc' })}
      </Description>
    </div>
  </div>
  </Container>
);

class Blog extends Component {
  render() {
    const _Blog = ({ intl: { formatMessage }}) => (
      <FlexedItems formatMessage={formatMessage} />
    );
    const Blog = inject('locale')(injectIntl(observer(_Blog)));
    return <Blog />;
  }
}

export default Blog;
